//Shrink navbar after scrolling down
$(window).scroll(function() {
	'use strict';
	if ($(document).scrollTop() > 50) {
		$('nav').addClass('shrink');
	} else {
		$('nav').removeClass('shrink');
	}
});

//Enable tooltips
$(document).ready(function(){
	'use strict';
    $('[data-toggle="tooltip"]').tooltip({
		animation: true,
		delay: 50
	}); 
});

//Changing active links
//Zelda Zelda Zelda
$(document).ready(function() {
	'use strict';
	$(".navbar-nav li a").click(function () {
		$(".navbar-nav li a").removeClass("active");
		$(this).addClass("active");
	});
});

//Rupees for days
$(document).ready(function() {
	$(document).on('scroll', onScroll);
	
	//Scroll smoothly
	$('a[href^="#"]').on('click', function(e) {
		e.preventDefault();
		$(document).off("scroll");
		
		$('.navbar-nav li a').each(function() {
			$(this).removeClass('active');	
		});
		
		$(this).addClass('active');
		
		var target = this.hash,
			menu = target;
		$target = $(target);
		$('html, body').stop().animate({
			'scrollTop': $target.offset().top
			}, 750, 'swing', function() {
				window.location.hash = target;
				$(document).on('scroll', onScroll);
		});
	});
});

//Page transition code is no longer here!!!!!
function onScroll(event) {
	'use strict';
	var scrollPos = $(document).scrollTop();
	$(".navbar-nav li a").each(function() {
		var currLink = $(this);
		var refElement = $(currLink.attr("href"));
		if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos)
			{
				$(".navbar-nav li a").removeClass('active');
				$(".navbar-brand").removeClass('active');
				currLink.addClass('active');
			}
			else {
				currLink.removeClass('active');
		}
	});
}